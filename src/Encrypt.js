import './common.css';
import { useState } from 'react';
import JSEncrypt from 'jsencrypt';

function Encrypt() {
  const [formValues, setfomrValues] = useState({
    key: '',
    data: ''
  });
  const [encryptedText, setEncryptedText] = useState('');

  const handleChange = (event) => {
    setfomrValues({
      ...formValues,
      [event.target.name]: event.target.value
    });
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();

    const encrypt = new JSEncrypt();
    encrypt.setPublicKey(formValues.key);
    const encrypted = encrypt.encrypt(formValues.data);
    setEncryptedText(encrypted);
  }

  return (
    <div className='view'>
      <header>
        <h3>
          Encrypt your data
        </h3>
      </header>
      <form onSubmit={handleSubmit}>
        <label>
          Raw Data:
          <textarea
            name='data'
            rows={10}
            value={formValues.data}
            onChange={handleChange}
          />
        </label>
        <label>
          Public Key:
          <textarea
            name='key'
            type='text'
            value={formValues.key}
            rows={6}
            placeholder='Starts with "-----BEGIN PUBLIC KEY-----"'
            onChange={handleChange}
          />
        </label>
        <button type='submit'>Encrypt</button>
      </form>

      {encryptedText
        ? <div>
          <b>Voila, here's your encrypted data:</b>
          <p>{encryptedText}</p>
        </div>
        : null  
      }
    </div>
  );
}

export default Encrypt;
