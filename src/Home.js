import { useState } from 'react';
import { Link } from 'react-router-dom';
import JSEncrypt from 'jsencrypt';

function Home() {
  const [keypair, setKeypair] = useState({});

  const handleGenerateKeypair = async () => {
    const crypt = new JSEncrypt({default_key_size: 1024});
    crypt.getKey();

    setKeypair({
      public: crypt.getPublicKey(),
      private: crypt.getPrivateKey()
    });
  }

  return (
    <div className='view'>
      <h3>
        Got a key pair?
      </h3>
      <p>
        <Link to='/encrypt'>Encrypt</Link>
        &nbsp;or&nbsp;
        <Link to='/decrypt'>decrypt</Link>
        &nbsp;some data!
      </p>
      <p>...OR <button onClick={handleGenerateKeypair}>Generate one (experimental)</button></p>
      {keypair.public
        ? (
          <div className='keys'>
            <p>
              <b>Public:</b>
              <br/>
              <textarea rows={6} readOnly={true} value={keypair.public} />
            </p>
            <p>
              <b>Private:</b>
              <br/>
              <textarea rows={15} readOnly={true} value={keypair.private} />
            </p>
          </div>
        )
        : null
      }
    </div>
  );
}

export default Home;
