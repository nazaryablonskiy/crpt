import './common.css';
import { useState } from 'react';
import JSEncrypt from 'jsencrypt';

function Decrypt() {
  const [formValues, setfomrValues] = useState({
    key: '',
    data: ''
  });

  const handleChange = (event) => {
    setfomrValues({
      ...formValues,
      [event.target.name]: event.target.value
    });
  }
  
  const handleSubmit = (event) => {
    event.preventDefault();
    
    const decrypt = new JSEncrypt();
    decrypt.setPublicKey(formValues.key);
    const decrypted = decrypt.decrypt(formValues.data);
    alert(decrypted);
  }

  return (
    <div className='view'>
      <header>
        <h3>
          Decrypt your data
        </h3>
      </header>
      <form onSubmit={handleSubmit}>
        <label>
          Encrypted Data:
          <textarea
            name='data'
            rows={10}
            value={formValues.data}
            onChange={handleChange}
          />
        </label>
        <label>
          Private Key:
          <textarea
            name='key'
            type='text'
            value={formValues.key}
            rows={15}
            placeholder='Starts with "-----BEGIN RSA PRIVATE KEY-----"'
            onChange={handleChange}
          />
        </label>
        <button type='submit'>Decrypt</button>
      </form>
    </div>
  );
}

export default Decrypt;
