import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import Decrypt from './Decrypt';
import Encrypt from './Encrypt';
import Home from './Home';

function App() {
  return (
    <div>
      <Router>
        <nav>
          <ul>
            <li>
              <Link to='/'>Home</Link>
            </li>
            <li>
              <Link to='/encrypt'>Encrypt</Link>
            </li>
            <li>
              <Link to='/decrypt'>Decrypt</Link>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route path='/encrypt'>
            <Encrypt />
          </Route>
          <Route path='/decrypt'>
            <Decrypt />
          </Route>
          <Route path='/'>
            <Home />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
