import { render, screen } from '@testing-library/react';
import App from './App';

test('no time for tests, would be awesome to have tho', () => {
  render(<App />);
  const linkElement = screen.getByText(/Home/i);
  expect(linkElement).toBeInTheDocument();
});
